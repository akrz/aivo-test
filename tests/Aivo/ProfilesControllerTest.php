<?php

namespace Aivo\Tests;

use Silex\WebTestCase;

class ProfilesControllerTest extends WebTestCase
{
    /**
     * @inheritdoc
     */
    public function createApplication()
    {
        return require __DIR__.'/../../src/app.php';
    }

    /**
     * Test getPublicFacebookProfileAction method
     */
    public function testGetPublicFacebookProfileAction()
    {
        $client = $this->createClient();

        // test status code 200
        $facebookUserId = 123;
        $client->request('GET', '/profile/facebook/'.$facebookUserId);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $responseBody = json_decode($client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('last_name', $responseBody);
        $this->assertArrayHasKey('first_name', $responseBody);

        // test status code 400 - invalid id
        $facebookUserId = '896790658750asd';
        $client->request('GET', '/profile/facebook/'.$facebookUserId);
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }
}