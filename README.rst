Aivo test - Retrieve a user profile
==============

Installation
----------------------------

Clone this repository, then

.. code-block:: console

    $ cd path/to/project
    $ composer install

Composer will install all necessary dependencies

How to use
-----------------------------

.. code-block:: console

    $ php -S localhost:8888 -t web

Then, browse to http://localhost:8888/index.php/profile/facebook/{id}

How to run tests
--------------------------

.. code-block:: console

    $ vendor/bin/phpunit
