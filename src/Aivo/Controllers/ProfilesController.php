<?php

namespace Aivo\Controllers;

use Aivo\Services\FacebookService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ProfilesController
 */
class ProfilesController
{
    /** @var FacebookService $facebookService */
    private $facebookService;

    public function __construct(FacebookService $facebookService)
    {
        $this->facebookService = $facebookService;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getPublicFacebookProfileAction(Request $request)
    {
        $data = $this->facebookService->getPublicFacebookProfile($request->get('id'));

        return new JsonResponse($data['response'], $data['status_code']);
    }
}