<?php

namespace Aivo\Services;

use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;

/**
 * Class FacebookService
 * @package Aivo\Services
 */
class FacebookService
{
    private $fb;

    public function __construct($facebook_app_id, $facebook_app_secret)
    {
        $this->fb = new Facebook([
            'app_id' => $facebook_app_id,
            'app_secret' => $facebook_app_secret,
            'default_graph_version' => 'v2.9',
            'default_access_token' => $facebook_app_id.'|'.$facebook_app_secret,
        ]);
    }

    /**
     * Get public info from facebook profile id
     *
     * @param $id
     * @return array
     */
    public function getPublicFacebookProfile($id)
    {
        if (!is_numeric($id)) {
            return ['response' => 'Invalid id. Bad request', 'status_code' => 400];
        }

        try {
            $response = $this->fb->get(
                '/'.$id.'?fields=id, first_name, last_name, gender, cover, picture, age_range, locale, link, timezone, updated_time, verified'
            );
        } catch (FacebookResponseException $e) {
            return ['response' => $e->getMessage(), 'status_code' => 400];
        } catch (FacebookSDKException $e) {
            return ['response' => $e->getMessage(), 'status_code' => 500];
        }

        $profile = $response->getDecodedBody();

        return ['response' => $profile, 'status_code' => 200];
    }
}
