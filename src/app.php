<?php

use Silex\Application;
use Silex\Provider\AssetServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;
use Rpodwika\Silex\YamlConfigServiceProvider;

$app = new Application();
$app->register(new ServiceControllerServiceProvider());
$app->register(new AssetServiceProvider());
$app->register(new TwigServiceProvider());
$app->register(new HttpFragmentServiceProvider());
$app->register(new YamlConfigServiceProvider(__DIR__."/../config/parameters.yml"));

// add custom globals, filters, tags, ...
$app['twig'] = $app->extend('twig', function ($twig, $app) {
    return $twig;
});

// Services
$app['facebook.service'] = function($app) {
    return new Aivo\Services\FacebookService($app['config']['facebook_app_id'], $app['config']['facebook_app_secret']);
};

// Controllers as services
$app['profiles.controller'] = function($app) {
    return new Aivo\Controllers\ProfilesController($app['facebook.service']);
};
$app->get('/profile/facebook/{id}', 'profiles.controller:getPublicFacebookProfileAction');

return $app;
